import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";

import HomePage from "./pages/HomePage";
import Contact from "./pages/Contact";
import About from "./pages/About";
import Skill from "./pages/Skill";
import Error404 from "./pages/Error404";

export default class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpened: false,
            closeIcon: false,
        };

        this.navBar = this.navBar.bind(this);
    }

    navBar(){
        this.setState({ isOpened: !this.state.isOpened });
        this.setState({ closeIcon: !this.state.closeIcon });
    }



    renderRoute(Component, props) {
        return (
            <div>
                <header className="header-area">
                    <div className="header-area-inner desktop-header">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-12 col-sm-4">
                                    <div className="logo-area">
                                        <a href="home.html"><span className="text-logo">E B</span></a>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-4">
                                    <div className="menu-bar">
                                        <i className={ this.state.closeIcon == false ? "fa fa-bars" : "fa fa-times" } id="menu" onClick={this.navBar}></i>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-4">
                                    <div className="social-icon-area">
                                        <ul className="social-icon">
                                            <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i className="fa fa-github"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="header-area-inner mobile-header">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-6 col-sm-6">
                                    <div className="logo-area">
                                        <a href="home.html"><span className="text-logo">vento</span></a>
                                    </div>
                                </div>
                                <div className="col-xs-6 col-sm-6">
                                    <div className="menu-bar">
                                        <i className="fa fa-bars" id="mobile-menu"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div className={"navbar-area " + (this.state.isOpened == false ? "hide-menu" : "")}>
                    <div className="container">
                        <div className="row">
                            <nav className="navbar navbar-default">
                                <div className="container-fluid">
                                    <div id="navbar" className="navbar-collapse collapse">
                                        <ul className="nav navbar-nav">
                                            <li className="active"><Link to="/"> Home </Link></li>
                                            <li className="dropdown">
                                                <a href="#" className="dropdown-toggle" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="false">About <span
                                                    className="caret"></span></a>
                                                <ul className="dropdown-menu">
                                                    <li><a href="about.html"><i className="fa fa-user"></i> About Me</a>
                                                    </li>
                                                    <li><a href="history.html"><i
                                                        className="fa fa-hourglass-2"></i> History</a></li>
                                                    <li><a href="hobby.html"><i className="fa fa-child"></i> Hobby</a>
                                                    </li>
                                                    <li><a href="lazy.html"><i className="fa fa-gamepad"></i> Lazy Time</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li className="dropdown">
                                                <a href="#" className="dropdown-toggle" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="false">Resume <span
                                                    className="caret"></span></a>
                                                <ul className="dropdown-menu">
                                                    <li><a href="education.html"><i
                                                        className="fa fa-graduation-cap"></i> Education</a></li>
                                                    <li><a href="experience.html"><i
                                                        className="fa fa-briefcase"></i> Experience</a></li>
                                                    <li><a href="skills.html"> Skills</a>
                                                        <Link to="/skills"><i className="fa fa-gears"></i> Skills </Link>
                                                    </li>
                                                    <li><a href="awards.html"><i
                                                        className="fa fa-trophy"></i> Awards</a></li>
                                                </ul>
                                            </li>
                                            <li className="dropdown">
                                                <a href="#" className="dropdown-toggle" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true"
                                                   aria-expanded="false">Portfolio <span className="caret"></span></a>
                                                <ul className="dropdown-menu">
                                                    <li><a href="work.html"><i className="fa fa-folder-o"></i> Works</a>
                                                    </li>
                                                    <li><a href="work-detail.html"><i
                                                        className="fa fa-info-circle"></i> Work Detail</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="team.html">Team</a></li>
                                            <li className="dropdown">
                                                <a href="#" className="dropdown-toggle" data-toggle="dropdown"
                                                   role="button" aria-haspopup="true" aria-expanded="false">Blog <span
                                                    className="caret"></span></a>
                                                <ul className="dropdown-menu">
                                                    <li><a href="blog.html"><i className="fa fa-briefcase"></i> Blog
                                                        Post</a></li>
                                                    <li><a href="blog-detail.html"><i
                                                        className="fa fa-graduation-cap"></i> Blog Detail</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="404.html">404 Error</a></li>
                                            <li><a href="contact.html">Contact</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>

                <Component {...props} />
            </div>
        );
    }

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" render={(props) => this.renderRoute(HomePage, props)} />
                    <Route exact path="/about" render={(props) => this.renderRoute(About, props)} />
                    <Route exact path="/contact" render={(props) => this.renderRoute(Contact, props)} />
                    {/*<Route exact path="/detail/:lang" render={(props) => this.renderRoute(Detail, props)} />*/}
                    <Route exact path="/skills" render={(props) => this.renderRoute(Skill, props)} />
                    <Route path="*" render={(props) => this.renderRoute(Error404, props)} />

                    <div className="footer">
                        Footer
                    </div>
                </Switch>
            </BrowserRouter>
        );
    }
}
