import React, { Component } from "react";
import { Line, Circle } from 'rc-progress';

export default class Skill extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            percent: 0,
        };

        this.increase = this.increase.bind(this);
    }

    /*componentDidMount() {
        const language = this.props.match.params.lang;
        // fetch

        fetch(`/api/get-info/${language}`)
            .then((response) => response.json())
            .then((data) => {
                this.setState({
                    info: data.info,
                });
            })
            .catch((err) => {
                this.setState({
                    info: "Couldn't load info.",
                });
            });
    }*/

    componentDidMount(){
        this.increase();
    }

    increase() {
        const percent = this.state.percent + 1;
        if (percent >= 100) {
            clearTimeout(this.tm);
            return;
        }
        this.setState({ percent });
        this.tm = setTimeout(this.increase, 10);
    }

    render() {
        return (
            <div className="skills-area">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="section-title wow fadeIn">
                                <p><i className="fa fa-cogs"></i></p>
                                <h2 className="title">Skills</h2>
                            </div>
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <div className="skills-info-area">
                                <div className="row">
                                    <div className="skill-bar-item">
                                        <span className="title">Photoshop</span>
                                        {/*<div className="progress vertical bottom">*/}
                                            {/*<Line strokeWidth="1" className="lineee" percent={this.state.percent} />*/}
                                        {/*</div>*/}
                                        <Line strokeWidth="1" className="progress vertical bottom" percent={this.state.percent} />
                                        <span className="percentage">95%</span>
                                    </div>
                                    <div className="skill-bar-item">
                                        <span className="title">Illustrator</span>
                                        <div className="progress vertical bottom">
                                            <div className="progress-bar progress-bar-warning six-sec-ease-in-out" role="progressbar" data-transitiongoal="75"></div>
                                        </div>
                                        <span className="percentage">75%</span>
                                    </div>
                                    <div className="skill-bar-item">
                                        <span className="title">HTML/CSS</span>
                                        <div className="progress vertical bottom">
                                            <div className="progress-bar progress-bar-warning six-sec-ease-in-out" role="progressbar" data-transitiongoal="85"></div>
                                        </div>
                                        <span className="percentage">85%</span>
                                    </div>
                                    <div className="skill-bar-item">
                                        <span className="title">PHP</span>
                                        <div className="progress vertical bottom">
                                            <div className="progress-bar progress-bar-warning six-sec-ease-in-out" role="progressbar" data-transitiongoal="80"></div>
                                        </div>
                                        <span className="percentage">80%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
