import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class About extends Component {
    render() {
        return <div>
            <Link to="/detail/react">React</Link>
            <Link to="/detail/php">PHP</Link>
            <Link to="/detail/js">JavaScript</Link>
            <Link to="/detail/css">CSS</Link>
            <Link to="/detail/laravel">Laravel</Link>
        </div>;
    }
}
