import React, { Component } from "react";
import MyImage from "../../../img/profile-img.jpg";

export default class HomePage extends Component {
    render() {
        return (
            <div className="hero-area">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-md-6">
                            <div className="hero-area-caption">
                                <div className="caption-content">
                                    <h2 className="greetings wow fadeInLeft">Hello!</h2>
                                    <h1 className="lead wow zoomIn" data-wow-delay="0.5s"><span>I'm </span>Edmond Bokhyan
                                    </h1>
                                    <p className="sublead wow zoomIn" data-wow-delay="0.7s">Web Designer & Web Developer</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <img src={ MyImage } className="profile-img" alt=""/>
                        </div>
                    </div>
                </div>
                <div className="hero-area-footer">
                    <div className="hero-footer-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-12">
                                    <ul className="info wow fadeIn">
                                        <li><span className="criteria">AGE :</span><span className="desc">16</span></li>
                                        <li><span className="criteria">ADDRESS :</span><span className="desc">Yerevan, Armenia</span>
                                        </li>
                                        <li><span className="criteria">E-MAIL :</span><span
                                            className="desc">edmond.bokhyan@gmail.com</span></li>
                                        <li><span className="criteria">PHONE :</span><span className="desc">(374) 41-06-03-30</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
